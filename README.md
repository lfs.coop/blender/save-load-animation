# Save and Load animation
This Blender add-on adds a panel in the Graph Editor, allowing you to
save the selected animation keyframes to an external files, and
reapply them later.

You may either import the keyframes at their original locations, or
starting at the current frame.

## License

Blender scripts published by **Les Fées Spéciales** are, except where
otherwise noted, licensed under the GPLv2 license.
