# Copyright (C) 2021 Les Fées Spéciales
# voeu@les-fees-speciales.coop
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


bl_info = {
    "name": "Save and Load Animation",
    "author": "Les Fées Spéciales (LFS)",
    "version": (1, 0, 0),
    "blender": (2, 91, 0),
    "location": "Graph Editor > Sidebar > F-Curve > Save and Load Animation",
    "description": "Export and import animation to and from file",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Import-Export",
    }


import bpy
from bpy.props import StringProperty, PointerProperty, BoolProperty
from mathutils import Vector
import json
import os
from math import inf


class ANIMATION_OT_ExportAnimation(bpy.types.Operator):
    """Export Animation to File"""
    bl_idname = "animation.export_animation"
    bl_label = "Export Animation to File"

    filepath: StringProperty(options={'HIDDEN'})
    anim_data: StringProperty(options={'HIDDEN'})

    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def invoke(self, context, event):
        """Create animation data structure and check if it's empty. If so, cancel, else
        check if the file exists and ask for confirmation. Then run
        execute()
        """
        filepath = bpy.path.abspath(context.scene.import_export_animation_props.filepath)
        if not filepath:
            self.report({'ERROR'}, 'Please specify file path')
            return {'CANCELLED'}
        elif os.path.isdir(filepath):
            self.report({'ERROR'}, 'File path points to a directory. Please specify a file name')
            return {'CANCELLED'}

        obj = context.active_object
        if (obj.animation_data is None or obj.animation_data.action is None
                or len(obj.animation_data.action.fcurves) == 0):
            self.report({'ERROR'}, 'No animation found')
            return {'CANCELLED'}
        anim_data = {}
        first_frame = inf
        for fcurve in obj.animation_data.action.fcurves:
            # Ignore unselected curves
            if (fcurve not in context.selected_visible_fcurves
                    or not fcurve.select
                    or not any(point.select_control_point for point in fcurve.keyframe_points)):
                continue
            fcurve_data = {}
            points = []

            for keyframe_point in sorted(fcurve.keyframe_points, key=lambda k:k.co.x):
                # Ignore unselected points
                if not keyframe_point.select_control_point:
                    continue
                first_frame = min(first_frame, keyframe_point.co.x)
                point_data = {}
                point_data['co'] = keyframe_point.co.to_tuple()
                point_data['period'] = keyframe_point.period
                point_data['easing'] = keyframe_point.easing
                point_data['interpolation'] = keyframe_point.interpolation
                point_data['handle_left'] = keyframe_point.handle_left.to_tuple()
                point_data['handle_left_type'] = keyframe_point.handle_left_type
                point_data['handle_right'] = keyframe_point.handle_right.to_tuple()
                point_data['handle_right_type'] = keyframe_point.handle_right_type
                point_data['amplitude'] = keyframe_point.amplitude
                point_data['back'] = keyframe_point.back
                point_data['type'] = keyframe_point.type

                points.append(point_data)

            fcurve_data['points'] = points
            fcurve_data['first_frame'] = first_frame

            anim_data[f'{fcurve.data_path}.{fcurve.array_index}'] = fcurve_data

        if not anim_data:
            self.report({'ERROR'}, 'No keyframe found to export')
            return {'CANCELLED'}

        self.anim_data = json.dumps(anim_data, indent=2)
        self.filepath = filepath

        # File does not exist, create it
        if not os.path.isfile(filepath):
            print("executing")
            return self.execute(context)
        # Ask for confirmation
        else:
            return context.window_manager.invoke_confirm(self, event)

    def execute(self, context):
        os.makedirs(os.path.dirname(self.filepath), exist_ok=True)
        with open(self.filepath, 'w') as f:
            f.write(self.anim_data)
        return {'FINISHED'}


class ANIMATION_OT_ImportAnimation(bpy.types.Operator):
    """Import animation from file"""
    bl_idname = "animation.import_animation"
    bl_label = "Import Animation from File"
    bl_options = {'UNDO'}

    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def execute(self, context):
        obj = context.active_object
        if obj.animation_data is None:
            obj.animation_data_create()
        obj_anim_data = obj.animation_data
        if obj.animation_data.action is None:
            action = bpy.data.actions.new(obj.name + "Action")
            obj.animation_data.action = action
        obj_fcurves = obj.animation_data.action.fcurves

        file_path = bpy.path.abspath(context.scene.import_export_animation_props.filepath)
        if file_path and os.path.isfile(file_path):
            with open(file_path, 'r') as f:
                anim_data = json.load(f)
        elif os.path.isdir(file_path):
            self.report({'ERROR'}, 'File path points to a directory. Please specify a file name')
            return {'CANCELLED'}
        else:
            self.report({'ERROR'}, 'Please check that file exists')
            return {'CANCELLED'}

        first_frame = min(point['first_frame'] for point in anim_data.values())

        for fcurve, fcurve_data in anim_data.items():
            data_path = ".".join(fcurve.split(".")[:-1])
            array_index = int(fcurve.split(".")[-1])
            obj_fcurve = obj_fcurves.find(data_path, index=array_index)
            if obj_fcurve is None:
                obj_fcurve = obj_fcurves.new(data_path, index=array_index)

            for keyframe_point in fcurve_data['points']:
                co = Vector(keyframe_point['co'])
                if context.scene.import_export_animation_props.insert_at_current_frame:
                    co.x += context.scene.frame_current_final - first_frame

                point = obj_fcurve.keyframe_points.insert(*co, options={'REPLACE', 'FAST', 'NEEDED'}, keyframe_type=keyframe_point['type'])

                point.type = keyframe_point['type']
                point.period = keyframe_point['period']
                point.easing = keyframe_point['easing']
                point.interpolation = keyframe_point['interpolation']
                point.handle_left = keyframe_point['handle_left']
                point.handle_left_type = keyframe_point['handle_left_type']
                point.handle_right = keyframe_point['handle_right']
                point.handle_right_type = keyframe_point['handle_right_type']
                point.amplitude = keyframe_point['amplitude']
                point.back = keyframe_point['back']

            obj_fcurve.update()

        return {'FINISHED'}


class ImportExportAnimationProps(bpy.types.PropertyGroup):
    filepath: StringProperty(name='Filepath',
                             description="File to save to and load from",
                             subtype='FILE_PATH')
    insert_at_current_frame: BoolProperty(name='Current Frame',
                                          description="Insert animation "
                                          "starting at current frame", default=True)


class ANIMATION_PT_ImportExportAnimation(bpy.types.Panel):
    """Panel for animation import and export"""
    bl_label = "Save and Load Animation"
    bl_idname = "ANIMATION_PT_ImportExportAnimation"
    bl_category = "F-Curve"
    bl_space_type = 'GRAPH_EDITOR'
    bl_region_type = 'UI'

    def draw(self, context):
        layout = self.layout

        layout.prop(context.scene.import_export_animation_props, 'filepath')

        row = layout.row(align=True)

        col = row.column(align=True)
        col.label(text="")
        col.operator('animation.export_animation', icon='EXPORT')

        col = row.column(align=True)
        col.prop(context.scene.import_export_animation_props, 'insert_at_current_frame')
        col.operator('animation.import_animation', icon='IMPORT')


def register():
    bpy.utils.register_class(ANIMATION_OT_ExportAnimation)
    bpy.utils.register_class(ANIMATION_OT_ImportAnimation)
    bpy.utils.register_class(ANIMATION_PT_ImportExportAnimation)
    bpy.utils.register_class(ImportExportAnimationProps)
    bpy.types.Scene.import_export_animation_props = PointerProperty(type=ImportExportAnimationProps)


def unregister():
    del bpy.types.Scene.import_export_animation_props
    bpy.utils.unregister_class(ANIMATION_PT_ImportExportAnimation)
    bpy.utils.unregister_class(ANIMATION_OT_ExportAnimation)
    bpy.utils.unregister_class(ANIMATION_OT_ImportAnimation)
    bpy.utils.unregister_class(ImportExportAnimationProps)


if __name__ == "__main__":
    register()
